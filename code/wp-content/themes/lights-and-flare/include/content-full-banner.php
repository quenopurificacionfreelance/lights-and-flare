<?php
if( get_row_layout() == 'full_banner' ): 				        	
	if( have_rows('banner_repeater') ): ?>
		<div class="banner-container">
			<div class="fixed">
				<div id="main" role="main">
					<section class="slider">
						<div class="flexslider">
							<ul class="slides"> <?php
								while ( have_rows('banner_repeater') ) : the_row();
									$banner_image = get_sub_field('banner_image');	
									$add_text = get_sub_field('add_text');
									$banner_title = get_sub_field('text_1');
									$banner_details = get_sub_field('text_2');
									$banner_link = get_sub_field('page_link');
									//Content display
									if(!empty($banner_image)):
<<<<<<< HEAD
										$bannercontent = '<li data-0="background-position:0px 0px;" data-end="background-position:0px -150px;" class="fullbanner" style="background-image: url('.$banner_image[url].');"><div class="middle" >';
=======
										$bannercontent = '<li data-0="background-position:0px -50px;" data-end="background-position:0px -500px;" class="fullbanner" style="background-image: url('.$banner_image[url].');"><div class="middle" >';
>>>>>>> b33af4cee67e0e3f70af2fcb40133f61f4c5666f
										if(!empty($add_text)):
											if(!empty($banner_title)):
												if(!empty($banner_link)):
													$bannercontent .= '<a href="'.$banner_link.'">';
												endif;
												$bannercontent .= '<h2 class="animated2">'.$banner_title.'</h2>';
												if(!empty($banner_link)):
													$bannercontent .= '</a>';
												endif;
											endif;
											if(!empty($banner_details)):
												$bannercontent .= '<div class="animated2">'.$banner_details.'</div>';											
												$bannercontent .= '<div class="animated2"><a href="'.$banner_link.'">Read more</a></div>';
											endif;
										endif;
										$bannercontent .='</div></li>';
										echo $bannercontent;
									endif;
								endwhile; ?>							
							</ul>
						</div>
					</section>
				</div>
			</div>
		</div>		
		<?php
	endif;
endif;
