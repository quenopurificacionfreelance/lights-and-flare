<?php $terms = get_sub_field('taxonomy'); ?>
	<div class="featured-gallery">
		<div class="container">
			<?php if ( !is_front_page() ) : ?>
			<div class="gallery-container">
				<ul class="filter_stories">					
					<li class="filters-select init">Show All</li>
						<span>
						<li class="item-hidden raven option" data-value="*" value="*">Show All</li>				
						<?php 
							foreach( $terms as $term ):
								$termid = $term->term_id;										
								$cat = $termid.",". + $cat;
								$catname = strtolower($term->name);													
								echo '<li value="'.$catname.'" class="item-hidden '.$catname.'" data-value=".'.$catname.'">'.$term->name.'</li>';
							endforeach;
						?>			
						</span>
				</ul>
			</div>
			<?php endif; ?>
			<div class="clearfix"></div>
			<div class="grid">
			<?php 
				foreach( $terms as $term2 ):
					$termid2 = $term2->term_id;										
					$cat2 = $termid2.",". + $cat2;											
				endforeach;
			?>	
				<?php									
				//echo $cat2;

				$args2 = array(
					'posts_per_page' => 999,
					'orderby' => date,
					'order' => DEC,
					'cat' => $cat2
				);
				query_posts($args2); 

				if ( have_posts() ) : 
					while ( have_posts() ) : the_post();

						$postcat = get_the_category();
						foreach( $postcat as $singlecat ):
							$singlecatname = strtolower($singlecat->name);

							$singlecatnamedisplay = $singlecatnamedisplay." ".$singlecatname;
						endforeach;
						if( has_post_thumbnail() ): 
							//$featured_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured-thumbnail' );
							// echo "<div class='col-md-4 col-sm-4 col-xs-4 box element-item' data-0='background-position:0px 0px;' data-end='background-position: 0px -3000px;'  style='background: url(".$featured_image_url[0].");'>";
							echo "<div class='col-md-4 col-sm-4 col-xs-6 box element-item ".$singlecatnamedisplay."'>"; ?>

							<div class="view">
								<!-- Image  -->
								<figure class="item_img img-intro img-intro__left">
									<a class="touchGalleryLink zoom galleryZoomIcon" href="<?php the_permalink(); ?>">   
										<?php the_post_thumbnail('medium-size', array( 'class'	=> "img-responsive")); ?>										
									</a>
								</figure>    
								<div class="mask">
									<div class="mask_wrap">
										<div class="mask_cont">
											<!--  title/author -->
											<div class="item_header">
												<h4 class="item_title">
													<a href="<?php the_permalink(); ?>"> 
														<?php the_title(); ?>
													</a>
												</h4>
											</div>											
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>

							<?php
								
							echo "</div>";
						endif;

						$singlecatnamedisplay = '';
					endwhile;
				endif; ?>
			</div>
		</div> 
	</div>

