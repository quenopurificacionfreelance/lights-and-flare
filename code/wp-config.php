<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lightsandflair');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'P@ssw0rd');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6bl31:2<pvHA?>:n X}XPfp}5era112AIQby&|*+i@,{Z8!P=%R m_6?vi80w82 ');
define('SECURE_AUTH_KEY',  '@yERQajJc!d%(PbA]E8},iB[#d;g63-eJ<j3BG3PUW=4bN-xgl[+w&8bQJ|oc=^Q');
define('LOGGED_IN_KEY',    'd]=rN3vU])q|VgWx)W8GMS$^nG$}Ew3-@du#0_0/8#@0y.CtgZEvDjwgF;l.||l~');
define('NONCE_KEY',        'b3<T?LMqd,@va5j?bakQtBroO|M=c1#gb9e@2m[AfWL)u>q=ET,`o:+Jw.-XLLHu');
define('AUTH_SALT',        '|xeN? Q-5H<|!R<<}P/<F}k}u#My+,d_M]HO-NE[IxYe=lWEw5%iV#|NMNy3ig-6');
define('SECURE_AUTH_SALT', 'WAy(;{2Ed[_Z|Cft4y A.kAR]0?8t3+|:+37AHu-[rd3&Qj [^O~$nP|KAI-0wMM');
define('LOGGED_IN_SALT',   'pB7]Cx-Wf-D|E{$:|5ZWUEc-]UeQWQ}/8/H Iw.UFdb5!jja.-H0uEuUu)W_Zqx{');
define('NONCE_SALT',       '1;5QGvzM}Mf4;+.ZZINy$._y_gWd_]AX}.B=qrMSfCc@CrzcTwb&CD&hiBaaH%YW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lf_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
