<?php
show_admin_bar( false );
register_nav_menus( array(
	'navigation_menu_left' => 'Header Navigation Menu Left',	
	'navigation_menu_right' => 'Header Navigation Menu Right',	
	'footer_menu' => 'Footer Menu',
) );

function widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Search', 'lights-and-flare' ),
		'id'            => 'search',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="hidden">',
		'after_title'   => '</span>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer', 'lights-and-flare' ),
		'id'            => 'footer',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="front-tag">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'widgets_init' );

function post_thumbnail() {
 //Featured Image
	add_theme_support('post-thumbnails');
	add_image_size( 'featured-thumbnail', 335, 223, true );
	add_image_size( 'small-thumbnail', 105);
	add_image_size( 'normal-thumbnail', 250);
	add_image_size( 'medium-size', 480);
}
add_action('after_setup_theme', 'post_thumbnail');

// add tag support to pages
function tags_support_all() {
 register_taxonomy_for_object_type('post_tag', 'page');
}
// ensure all tags are included in queries
function tags_support_query($wp_query) {
 if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');

//require get_template_directory() . '/include/cpt-slider.php';
//require get_template_directory() . '/include/cpt-solutions-pathways.php';

require get_template_directory() . '/include/acf-options-cpt.php';
 
?>