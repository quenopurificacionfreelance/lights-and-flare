<?php get_header(); ?>
	<div class="page-content">
		<div class="banner">			
				<?php 
					// check if the flexible content field has rows of data
					if( have_rows('banner_options') ):
						wp_reset_query();
						while ( have_rows('banner_options') ) : the_row();				
					    	include 'include/content-full-banner.php';
					    	wp_reset_query();

					    	include 'include/content-banner.php';
					    	wp_reset_query();

					    	include 'include/content-collage-banner.php';
					    	wp_reset_query();

					    	include 'include/content-map-banner.php';
					    	wp_reset_query();
					    	
					    endwhile;
					endif;
				?>
				</div>
			</div>
		</div>
		<div class="content main">	
			<div class="white">
				<div class="container">
					<?php 
						$pagetitle = get_the_title();
						echo "<h1>".$pagetitle."</h1>";
					?>
				</div>
			</div>
			<div class="container">
				<div class="tab-content">
					<div id="single-tab">
						<?php
							$content = get_field('content');
							$gallery = get_field('gallery');
							$post_video = get_field('post_video');
							$map = get_field('map');
						?>
					<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<?php if( $content ): ?>
								<li role="presentation"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">About / </a></li>
							<?php endif; ?>
							<?php if( $gallery ): ?>
								<li role="presentation"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">Photos / </a></li>
							<?php endif; ?>
							<?php if( $post_video ): ?>
								<li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Videos / </a></li>
							<?php endif; ?>
							<?php if( $map ): ?>
								<li role="presentation" class="active"><a href="#map" aria-controls="map" role="tab" data-toggle="tab">Map</a></li>
							<?php endif; ?>						
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
						
							<div role="tabpanel" class="tab-pane" id="about">
								<?php echo $content; ?>
							</div>
							
							<div role="tabpanel" class="tab-pane" id="photos">
								<?php  									
									if( $gallery ): ?>									   
									        <?php foreach( $gallery as $image ):
									            echo "<div class='col-md-8 col-sm-8 col-xs-8 col-md-offset-2 col-sm-offset-2'><div class='row'>"; ?>
									                <a href="<?php echo $image['url']; ?>">
									                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive"/>
									                </a>								                
									            <?php
									            echo "</div></div>"; ?>
									        <?php endforeach; ?>								   
									<?php endif;
								?>
							</div>	

							<div role="tabpanel" class="tab-pane embed-container" id="videos">
								<div class="col-md-8 col-sm-8 col-md-offset-2"> 
								<?php 
									// use preg_match to find iframe src
									preg_match('/src="(.+?)"/', $post_video, $matches);
									$src = $matches[1];
									// add extra params to iframe src
									$params = array(
									'controls'    => 1,
									'hd'        => 1,
									'autohide'    => 1
									);
									$new_src = add_query_arg($params, $src);
									$post_video = str_replace($src, $new_src, $post_video);
									// add extra attributes to iframe html
									$attributes = 'frameborder="0"';
									$post_video = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $post_video);
									// echo $post_video
									echo $post_video;
								?>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane active" id="map">
								<?php
								if($map):
									$loc1 = '<div class="innerbanner-container"><div class=""><div class="inner-banner">';
									
									$loc1 .= '<div class="acf-map">';
									$loc1 .= '<div class="marker" data-lat="'.$map['lat'].'" data-lng="'.$map['lng'].'">';
									$loc1 .= '<p class="address">'.$map['address'].'</p>';
									$loc1 .= '</div>';
									$loc1 .= '</div>';	
									$loc1 .= '</div></div></div>';
									$loc1 .= '<div class="clearfix"></div>';
									echo $loc1; 
								endif; ?>								
							</div>
							<div class="clearfix"></div>
						</div>

					</div>
				</div>		
			</div>							
		</div>
	</div>
<?php get_footer(); ?>