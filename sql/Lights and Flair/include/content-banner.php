<?php
if( get_row_layout() == 'banner' ): 
	$inner_banner = get_sub_field('inner_banner');
	$banner_align = get_sub_field('banner_align');	
	
	if(!empty($inner_banner)) :
		$bannerbg = "<div class='innerbanner-container'><div class='fixed'><div class='inner-banner' data-0='background-position:0px ".$banner_align."px;' data-end='background-position:0px -300px;' style='background-image: url(".$inner_banner[url].");'></div></div></div>";
		echo $bannerbg;		
	endif; ?>
	<!-- <div class="bubbleframe">
		<div class="divider-2"></div>
	</div> -->	
	<?php
endif;
