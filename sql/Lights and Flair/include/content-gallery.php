<?php
echo "<div class='gallery-single'><div class='container'><div class='grid-sizer'></div>";
$images = get_sub_field('any_gallery');
	if( $images ): ?>									   
	        <?php foreach( $images as $image ):
	            echo "<div class='col-md-8 col-sm-8 col-xs-8 col-md-offset-2 col-sm-offset-2'><div class='row'>"; ?>
	                <a href="<?php echo $image['url']; ?>">
	                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive"/>
	                </a>								                
	            <?php
	            echo "</div></div>"; ?>
	        <?php endforeach; ?>								   
	<?php endif; 
echo "</div></div>";
?>