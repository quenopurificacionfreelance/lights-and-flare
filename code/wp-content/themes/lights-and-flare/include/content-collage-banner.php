<?php
if( get_row_layout() == 'collage_gallery_banner' ): 
	$gallery_pictures = get_sub_field('gallery_pictures');
	
	

	if($gallery_pictures) :
		echo "<div class='innerbanner-container'><div class='fixed'><div class='inner-banner3 grid2'><div class='grid-sizer'></div>";		
			if( $gallery_pictures ): ?>									   
			        <?php foreach( $gallery_pictures as $image ):
			            echo "<div class='grid-item col-md-2 col-sm-2 col-xs-3' data-0='background-position:-20px 0px;' data-end='background-position:-300px 0px;'  style='background: url(".$image['sizes']['normal-thumbnail'].");'><div class='row'>"; ?>
			                <a href="<?php echo $image['url']; ?>">
			                     <img src="<?php echo $image['sizes']['normal-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive visibility-h"/>
			                </a>								                
			            <?php
			            echo "</div></div>"; ?>
			        <?php endforeach; ?>								   
			<?php endif; 
		echo "</div></div></div>";
		wp_reset_query();
	endif; ?>
	<!-- <div class="bubbleframe">
		<div class="divider-2"></div>
	</div> -->
	
	<?php	
endif;
