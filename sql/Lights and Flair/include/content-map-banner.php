<?php
if( get_row_layout() == 'map_banner' ): 
	$location_map = get_sub_field('map');
	$loc1 = '<div class="innerbanner-container"><div class="fixed"><div class="inner-banner">';
	
	$loc1 .= '<div class="acf-map">';
	$loc1 .= '<div class="marker" data-lat="'.$location_map['lat'].'" data-lng="'.$location_map['lng'].'">';
	$loc1 .= '<p class="address">'.$location_map['address'].'</p>';
	$loc1 .= '</div>';
	$loc1 .= '</div>';	
	$loc1 .= '</div></div></div>';
	$loc1 .= '<div class="clearfix"></div>';
	echo $loc1; ?>

<?php endif; ?>