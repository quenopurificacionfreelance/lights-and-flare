<?php 
if( get_row_layout() == 'divider' ): 
	$divider = get_sub_field('content_spacer'); 
	if(!empty($divider)):
?>
<div class="white">
	<div class="divider">				
	</div>
</div>
<?php endif; endif; ?>