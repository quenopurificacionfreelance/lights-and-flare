<?php
if( get_row_layout() == 'collage_gallery_banner' ): 
	$gallery_pictures = get_sub_field('gallery_pictures');
	
	foreach( $gallery_pictures as $loop ):
		$loopid = $loop->term_id;										
		$termloop = $loopid.",". + $termloop;
		
	endforeach;
	$gallerybanner = array(
		'posts_per_page' => 50,
		'orderby' => date,
		'order' => DEC,
		'cat' => $termloop
	);
	query_posts($gallerybanner); 

	if($gallery_pictures) :
		echo "<div class='innerbanner-container'><div class='fixed'><div class='inner-banner3 grid2'><div class='grid-sizer'></div>";
			
		while ( have_posts() ) : the_post();				
			$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
			if ( has_post_thumbnail() ) : // check if the post has a featured thumbnail assigned to it.
				
				echo "<div class='grid-item col-md-2 col-sm-2 col-xs-2'><div class='row'><a href='".$full_image_url[0]."' title=''>";
					the_post_thumbnail('normal-thumbnail', array( 'class' => "img-responsive")); //add class to featured thumbnail
				echo "</a></div></div>";						
				
			endif;

			//$singlecatnamedisplay = '';
		endwhile;
		
		echo "</div></div></div>";
		wp_reset_query();
	endif; ?>
	<div class="white">
		<div class="container">
			<?php 
				$pagetitle = get_the_title();
				echo "<h1>".$pagetitle."</h1>";
			?>
		</div>
	</div>
	<?php	
endif;
