		<footer>
			<div class="contact-form">
				<div class="container">
					<div class="row">
						
						<?php 						
							$form_object = get_field('choose_form', 'option');	
							$hssform = '<div class="form-hss">';
							
								$hssform .= '<div class="col-md-12 col-md-12"><div class="form-box">';
							
							$hssform .= do_shortcode('[gravityform id="' . $form_object['id'] . '" title="true" description="true" ajax="true"]');	
							$hssform .= '</div></div><div class="clearfix"></div></div>';
							echo $hssform;
						
						?>
					</div>
				</div>
			</div>
			<div class="copy-and-social">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<p><?php the_field('text', 'option'); ?></p>
						</div>
						<div class="col-md-6 col-sm-6">
							<?php 
								$social = get_field('social_icons', 'option'); 
								if($social) :
								while(has_sub_field('social_icons', 'option')): ?>
								<?php 
									$footer_image = get_sub_field('image_icon');
									$footer_link = get_sub_field('social_link'); 
									echo "<div class='social-icon'><a href='".$footer_link."' target='_blank'><img src='".$footer_image['url']."'></a></div>";
								?>								
							<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
<?php wp_footer(); ?>	
</body>
</html>