$(window).load(function(){
  var s = skrollr.init({
    edgeStrategy: 'set',
    easing: {
      WTF: Math.random,
      inverted: function(p) {
        return 1-p;
      }
    }
  });
  $("#loader-wrapper").fadeOut("slow");
  $('.grid2').isotope({
      itemSelector: '.grid-item',
      percentPosition: true,
      masonry: {
      // use outer width of grid-sizer for columnWidth
        columnWidth: '.grid-sizer'
      }
  });

    var windowWidth = $(window).width(); 
    var windowHeight = $(window).height();    
    $('li.fullbanner').height(windowHeight);
    $('.banner .banner-container').height(windowHeight);

    $("#gform_wrapper_1 input, #gform_wrapper_1 select, #gform_wrapper_1 textarea, #gform_wrapper_1 select").each(function(index, elem) {
      var eId = $(elem).attr("id");
      var label = null;
      if (eId && (label = $(elem).parents("form").find("label[for="+eId+"]")).length == 1) {
    $('.gfield_required').remove();
          $(elem).attr("placeholder", $(label).html());
          $(label).remove();
      }
    });

    var ws = $(window).width();
    var window_top = $(window).scrollTop() + 81;
    var div_top = $('.content').offset().top;
        if (window_top > div_top) {
          $('.logo a.primary img').removeClass('zoomIn');
            $('.logo a.primary img').addClass('zoomOut');

            $('.logo a.secondary img').removeClass('zoomOut hide');  
            $('.logo a.secondary img').addClass('zoomIn');
            
        } else {
            $('.logo a.primary img').removeClass('zoomOut');
            $('.logo a.primary img').addClass('zoomIn');

            $('.logo a.secondary img').removeClass('zoomIn');  
            $('.logo a.secondary img').addClass('hide');          
        }
  setInterval(function() {        
      $('.flexslider ul.slides li h2').removeClass('fadeInUp');
      $('.flexslider ul.slides li.flex-active-slide h2').addClass('fadeInUp');
      $('.flexslider ul.slides li div').removeClass('fadeInLeft');
      $('.flexslider ul.slides li.flex-active-slide div').addClass('fadeInLeft');
      $('.flexslider ul.slides li img').removeClass('fadeInRight');
      $('.flexslider ul.slides li.flex-active-slide img').addClass('fadeInRight');
  },100);

  $('#single-tab a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  })
});
