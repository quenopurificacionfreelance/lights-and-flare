<!doctype html>
<html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title><?php wp_title( '|', true, 'right' ); ?>Website Name</title>
<link href='https://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/animate.css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css" />

<!--[if IE 8]>
	<link type="text/css" rel="stylesheet" media="screen" href="<?php bloginfo('template_directory'); ?>/css/ie8.css" >
<![endif]-->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/skrollr.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.flexslider-min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjX7c0OjwLWJbKPzJ4Eq6hTxDL1AU67s4"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/contact-map.js"></script>
<?php 
	if(is_single()) : ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/tab.js"></script>
	<?php
	endif;
?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/custom.js"></script>

<?php 
	if(is_single()) : ?>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/tab.js"></script>
	<?php
	endif;
?>

<script type="text/javascript">	
$(window).load(function(){
	var s = skrollr.init({
		edgeStrategy: 'set',
		easing: {
			WTF: Math.random,
			inverted: function(p) {
				return 1-p;
			}
		}
	});
	$("#loader-wrapper").fadeOut("slow");
});

$(window).on("load resize",function(){  
  	$('.grid2').isotope({
	  	itemSelector: '.grid-item',
	  	percentPosition: true,
	  	masonry: {
	    // use outer width of grid-sizer for columnWidth
	  		columnWidth: '.grid-sizer'
	  	}
	})
})
</script>
<?php
	if( have_rows('banner_options') ):
		while ( have_rows('banner_options') ) : the_row();				
	    	if( get_row_layout() == 'full_banner' ): 				        	
				if( have_rows('banner_repeater') ): ?>
					<script type="text/javascript">	
						$(window).load(function(){
							$('.flexslider').flexslider({
								animation: "fade"
							});
						});
					</script>
				<?php
				endif;
			endif;
	    endwhile;
	endif;

	if( have_rows('flexible_content') ): 
		while ( have_rows('flexible_content') ) : the_row(); 
			if( get_row_layout() == 'featured_post' ): ?>
				<script type="text/javascript">	
					$(window).on("load resize",function(){
						//console.log($('ul.filter_stories'));
						$( function() {
						// init Isotope
						var $grid = $('.grid').isotope({
							itemSelector: '.element-item',
							layoutMode: 'fitRows'
						});
						// filter functions
						var filterFns = {
							// show if number is greater than 50
							numberGreaterThan50: function() {
								var number = $(this).find('.number').text();
								return parseInt( number, 10 ) > 50;
							},
								// show if name ends with -ium
							ium: function() {
								var name = $(this).find('.name').text();
								return name.match( /ium$/ );
							}
						};

						$('ul.filter_stories li.init').on('click', function() {
							if($(this).hasClass('active')) {
								$(this).removeClass('active');
							}
							else {
								$(this).addClass('active');
							}
							$('ul.filter_stories span').slideToggle();
						});

						var allOptions = $('ul.filter_stories').children('li:not(.init)');
						$('ul.filter_stories').on('click', 'li:not(.init)', function() {
							allOptions.removeClass('selected');
							$('ul.filter_stories li.item-hidden').removeClass('selected');
							$(this).addClass('selected');
							$('ul.filter_stories').children('.init').html($(this).html());

							//alert($(this).attr('value'));
							$('ul.filter_stories span').slideUp();
							$('ul.filter_stories li.init').removeClass('active prenup wedding *');
							$('ul.filter_stories li.init').addClass($(this).attr('value'));
						});


						var filterValue;
							// var filterValue = '.group';
							// filterValue = filterFns[ filterValue ] || filterValue;
							// $grid.isotope({ filter: filterValue });
							// $('.filters-select.init').addClass('group').text('Group');

							// bind filter on select change
							$('ul.filter_stories').on( 'click', "li:not(.init)", function() {
								// get filter value from option value
								//alert("filterValue");
								filterValue = $(this).attr('data-value');
								// use filterFn if matches value
								filterValue = filterFns[ filterValue ] || filterValue;
								$grid.isotope({ filter: filterValue });
							});

						//Trigger click. This is for the forms
						var value = window.location.href.substring(window.location.href.lastIndexOf('=') + 1);
							//alert(value);
							if(value) {   
							//alert(value);
								$('ul.filter_stories li.item-hidden'+value).click();
								filterValue = value;   
								filterValue = filterFns[ filterValue ] || filterValue;
								$grid.isotope({ filter: filterValue });

							}
						});						
					});
					
				</script>
			<?php
			endif;
		endwhile;
	endif;
?>



<?php wp_head(); ?>
</head>
<body <?php body_class( $class ); ?>>
<div id="loader-wrapper">
    <!-- <div id="loader"></div> -->
    <div id="ballWrapper">
		<div id="ball"></div>
		<div id="ballShadow"></div>			
	</div>
</div>
	
	<div class="wrapper">
		<header>
			<div class="menu_and_logo">
				<div class="container">
					<div class="logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="primary"><img src="<?php the_field('logo_image', 'option'); ?>" alt="<?php the_field('image_name', 'option'); ?>" class="animated zoomIn"></a>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="secondary"><img src="<?php the_field('secondary_logo', 'option'); ?>" alt="<?php the_field('image_name', 'option'); ?>" class="animated hide"></a>						
					</div>
					<nav>
						<div class="col-md-5 col-sm-5">
							<?php wp_nav_menu( array( 'theme_location'=>'navigation_menu_left' ) ); ?>
						</div>
						<div class="col-md-2 col-sm-2"></div>
						<div class="col-md-5 col-sm-5">
							<?php wp_nav_menu( array( 'theme_location'=>'navigation_menu_right' ) ); ?>
						</div>
					</nav>
				</div>
			</div>
		</header>