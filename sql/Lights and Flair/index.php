<?php get_header(); ?>
	<div class="page-content">
		<div class="banner">			
				<?php 
					// check if the flexible content field has rows of data
					if( have_rows('banner_options') ):
						wp_reset_query();
						while ( have_rows('banner_options') ) : the_row();				
					    	include 'include/content-full-banner.php';
					    	wp_reset_query();

					    	include 'include/content-banner.php';
					    	wp_reset_query();

					    	include 'include/content-collage-banner.php';
					    	wp_reset_query();

					    	include 'include/content-map-banner.php';
					    	wp_reset_query();
					    	
					    endwhile;
					endif;
				?>
				</div>
			</div>

		</div>
		<div class="content main">	
			
				<div class="white">
					<div class="container">
						<?php if( !is_page(7) ) : ?>
						<?php 
							$pagetitle = get_the_title();
							echo "<h1>".$pagetitle."</h1>";
						?>
						<?php else : ?>
							<h2>Featured Events</h2>
						<?php endif; ?>
					</div>
				</div>

			<?php
				if( have_rows('flexible_content') ): 
					while ( have_rows('flexible_content') ) : the_row(); 
						wp_reset_query();
						if( get_row_layout() == 'featured_post' ):
							include 'include/content-featured-gallery.php';							
						endif;

						//Gallery
						wp_reset_query();
						if( get_row_layout() == 'gallery' ):							
							include 'include/content-gallery.php';
						endif;

						wp_reset_query();
						if( get_row_layout() == 'full_text' ):							
							include 'include/content-fulltext.php';
						endif;

						wp_reset_query();
						if( get_row_layout() == 'divider' ):							
							include 'include/content-divider.php';
						endif;
					endwhile;
				endif;
			?>
		</div>
	</div>
<?php get_footer(); ?>