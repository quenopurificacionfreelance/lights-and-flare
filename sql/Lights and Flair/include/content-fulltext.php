<?php
if( get_row_layout() == 'full_text' ): 
	$content = get_sub_field('content');
	
	if(!empty($content)) :
		$bannerbg = "<div class='full-text'><div class='container'><div class='col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2'>";
		$bannerbg .= $content;
		$bannerbg .= "</div></div></div>";
		$bannerbg .= "<div class='clearfix'></div><div class='divider'></div>";
		echo $bannerbg;		
	endif; ?>
	
	<?php
endif;

